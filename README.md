# SEMCODT Event-B

Project modeling meta-model of Component based systems and the security properties using Event-B.

## Install

- Clone the repository:

```bash
# in the console (need git installed)
git clone git@gitlab.com:semcofdt/swarchitecture/cbsemps/event-b.git # Will create a directory 'event-b' in the current path 
cd event-b
```

- Open "src/" in [rodin editor](http://www.event-b.org/install.html)
